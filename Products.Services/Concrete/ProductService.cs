﻿namespace Products.Services.Concrete
{
    using System;
    using System.Collections.Generic;
    using Abstract;
    using Data.Models;
    using Data.Repository.Abstract;

    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public IEnumerable<Product> GetProducts()
        {
            return _productRepository.GetAll();
        }

        public void SaveProduct(Product product)
        {
            _productRepository.Save(product);
        }

        public Product GetProduct(int id)
        {
            return _productRepository.GetProduct(id);
        }

        public void DeleteProduct(int id)
        {
            _productRepository.Delete(id);
        }
    }
}