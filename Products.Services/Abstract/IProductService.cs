﻿namespace Products.Services.Abstract
{
    using System.Collections.Generic;
    using Data.Models;

    /// <summary>
    /// Interface for managing <see cref="Product"/> in system
    /// </summary>
    public interface IProductService
    {
        /// <summary>
        /// Get all <see cref="Product"/> entities from repository
        /// </summary>
        /// <returns></returns>
        IEnumerable<Product> GetProducts();

        /// <summary>
        /// Save <see cref="Product"/> entities
        /// </summary>
        /// <param name="product"></param>
        void SaveProduct(Product product);

        /// <summary>
        /// Get specified <see cref="Product"/> entities from repository
        /// </summary>
        /// <param name="id">Key of <see cref="Product"/> entity</param>
        /// <returns></returns>
        Product GetProduct(int id);

        /// <summary>
        /// Delete specified <see cref="Product"/> entities from repository
        /// </summary>
        /// <param name="id">Key of <see cref="Product"/> entity</param>
        void DeleteProduct(int id);
    }
}