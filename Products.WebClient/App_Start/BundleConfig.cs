﻿namespace Products.WebClient.App_Start
{
    using System;
    using System.Linq;
    using System.Web.Optimization;

    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            RegisterJsScripts(bundles);
            RegisterStyles(bundles);
        }

        private static void RegisterStyles(BundleCollection bundles)
        {
            Action<string, string[]> NewBundle =
                (s, strings) =>
                    bundles.Add(new StyleBundle("~/" + s).Include(strings.Select(s1 => "~/" + s1).ToArray()));

            NewBundle("bootstrap", new[]
            {
                "content/bootstrap.min.css",
                "content/main.css"
            });
        }

        private static void RegisterJsScripts(BundleCollection bundles)
        {
            Action<string, string[]> NewBundle =
                (s, strings) =>
                    bundles.Add(new ScriptBundle("~/" + s).Include(strings.Select(s1 => "~/" + s1).ToArray()));

            NewBundle("jquery", new[]
            {
                "scripts/jquery-{version}.js",
                "scripts/views/index.js"
            });

            NewBundle("ajax", new[]
            {
                "scripts/jquery.unobtrusive-ajax.js"
            });

            NewBundle("angular", new[]
            {
                "scripts/angular.min.js",
                "scripts/angular-route.min.js",
                "scripts/angular-resource.min.js"
            });

            NewBundle("prodapp", new[]
            {
                "scripts/app/app.js",
                "scripts/app/controllers.js"
            });
        }
    }
}