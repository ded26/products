﻿namespace Products.WebClient
{
    using System;
    using System.Data.Entity;
    using System.Web;
    using System.Web.Http;
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;
    using App_Start;
    using Data.EntityFramework;

    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            //Set default db initializer
            Database.SetInitializer(new ProductsDbInitializer());

            // Code that runs on application startup
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            //Register bundles
            BundleConfig.RegisterBundles(BundleTable.Bundles);            
        }
    }
}