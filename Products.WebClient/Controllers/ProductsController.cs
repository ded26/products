﻿namespace Products.WebClient.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Http;
    using Data.Models;
    using Models;
    using Services.Abstract;

    public class ProductsController : ApiController
    {
        private readonly IProductService _productService;

        public ProductsController(IProductService productService)
        {
            _productService = productService;
        }

        // GET: api/Products
        public IHttpActionResult Get()
        {
            try
            {
                return Json(_productService.GetProducts()
                        .Select(p => new ProductViewModel
                        {
                            Id = p.Id,
                            Name = p.Name,
                            Description = p.Description,
                            Price = p.Price,
                            Count = p.Count
                        }));
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            
        }

        // GET: api/Products/5
        public IHttpActionResult Get(int id)
        {
            try
            {
                var product = _productService.GetProduct(id);
                var model = new ProductViewModel
                {
                    Id = product.Id,
                    Name = product.Name,
                    Description = product.Description,
                    Count = product.Count,
                    Price = product.Price
                };
                return Ok(model);
            }
            catch
            {
                return NotFound();
            }
            
        }

        // POST: api/Products
        public IHttpActionResult Post(ProductViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var product = new Product
                    {
                        Id = model.Id,
                        Name = model.Name,
                        Description = model.Description,
                        Price = model.Price,
                        Count = model.Count
                    };
                    _productService.SaveProduct(product);
                    return Ok();
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
            return Json(model);
        }

        // PUT: api/Products/5
        public IHttpActionResult Put(int id, [FromBody] ProductViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var product = new Product
                    {
                        Id = id,
                        Name = model.Name,
                        Description = model.Description,
                        Price = model.Price,
                        Count = model.Count
                    };
                    _productService.SaveProduct(product);
                    return Ok();
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
            return Json(model);
        }

        // DELETE: api/Products/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                _productService.DeleteProduct(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
