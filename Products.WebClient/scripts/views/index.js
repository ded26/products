﻿$(document).ready(function () {
    var time = 200;
    $('#mod_show').click(function(event) {
        event.preventDefault();
        $('#overlay').fadeIn(time, function () {
            $('#modal')
                .css('display', 'block')
                .animate({ opacity: 1, top: '50%' }, time);
        });
    });

    $('#modal_close, #overlay').click(function() {
        $('#modal')
            .animate({ opacity: 0, top: '45%' }, time,
                function() {
                    $(this).css('display', 'none');
                    $('#overlay').fadeOut(time);
                });
    });
})