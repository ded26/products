﻿angular.module("ProdApp", ["ngRoute", "ngResource"])
    .config([
        "$locationProvider", "$routeProvider",
        function($locationProvider, $routeProvider) {
            $locationProvider.html5Mode({
                enabled: true,
                requireBase: false
            }).hashPrefix("!");

            $routeProvider
                .when("/", {
                    templateUrl: "/AngularViews/Products.html",
                    controller: "ProdController"
                })
                .when("/products/new", {
                    templateUrl: "/AngularViews/NewProduct.html",
                    controller: "NewProductController"
                });
        }
    ]).factory('Product', function ($resource) {
        return $resource('api/products/:id', { id: '@id' },
        {
            update: {
                method: 'PUT'
            }
        });
    });
    
