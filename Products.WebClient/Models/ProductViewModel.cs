﻿namespace Products.WebClient.Models
{
    using System.ComponentModel.DataAnnotations;

    public class ProductViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Enter product name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Enter product description")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Enter product price")]
        public double Price { get; set; }

        [Required(ErrorMessage = "Enter product count")]
        public int Count { get; set; }
    }
}