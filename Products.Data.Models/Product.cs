﻿namespace Products.Data.Models
{
    /// <summary>
    /// Db entity model
    /// </summary>
    public class Product
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public double Price { get; set; }

        public int Count { get; set; }
    }
}
