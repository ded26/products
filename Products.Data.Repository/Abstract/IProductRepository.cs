﻿namespace Products.Data.Repository.Abstract
{
    using System.Collections.Generic;
    using Models;

    /// <summary>
    /// Interface for manipulating <see cref="Product"/> entities
    /// </summary>
    public interface IProductRepository
    {
        /// <summary>
        /// Get all <see cref="Product"/> entities from db
        /// </summary>
        /// <returns></returns>
        IEnumerable<Product> GetAll();

        /// <summary>
        /// Add or update entities
        /// </summary>
        /// <param name="product"><see cref="Product"/> that should be added or changed</param>
        void Save(Product product);

        /// <summary>
        /// Get specified entities <see cref="Product"/> from db
        /// </summary>
        /// <param name="id">Id of <see cref="Product"/></param>
        /// <returns></returns>
        Product GetProduct(int id);

        /// <summary>
        /// Delete <see cref="Product"/> from db
        /// </summary>
        /// <param name="id">Id of <see cref="Product"/> which must be removed</param>
        void Delete(int id);
    }
}