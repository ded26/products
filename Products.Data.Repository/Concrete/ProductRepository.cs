﻿namespace Products.Data.Repository.Concrete
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using Abstract;
    using EntityFramework;
    using Models;

    /// <summary>
    /// Implement the <see cref="IProductRepository"/> interface
    /// </summary>
    public class ProductRepository : IProductRepository
    {
        private readonly ProductContext _productContext;

        public ProductRepository(ProductContext productContext)
        {
            _productContext = productContext;
        }

        public IEnumerable<Product> GetAll()
        {
            return _productContext.Products;
        }

        public void Save(Product product)
        {
            var dbEntry = _productContext.Products.Find(product.Id);
            if (dbEntry == null)
                _productContext.Products.Add(product);
            else
                _productContext.Entry(product).State = EntityState.Modified;
            _productContext.SaveChanges();
        }

        public Product GetProduct(int id)
        {
            var dbEntry = _productContext.Products.Find(id);
            return dbEntry;
        }

        public void Delete(int id)
        {
           
            var dbEntry = _productContext.Products.Find(id);
            if (dbEntry != null)
            {
                _productContext.Entry(dbEntry).State = EntityState.Deleted;
                _productContext.SaveChanges();
            }
        }
    }
}
