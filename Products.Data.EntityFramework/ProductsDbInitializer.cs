﻿namespace Products.Data.EntityFramework
{
    using System.Data.Entity;
    using Models;

    public class ProductsDbInitializer : DropCreateDatabaseIfModelChanges<ProductContext>
    {
        protected override void Seed(ProductContext context)
        {
            context.Products.Add(new Product { Name = "Ball", Description = "Football accessory", Price = 100, Count = 9 });
            context.Products.Add(new Product { Name = "Chess", Description = "Good for intelligence", Price = 60, Count = 3 });
            context.Products.Add(new Product { Name = "Bat", Description = "Baysball", Price = 40, Count = 6 });
            base.Seed(context);
        }
    }
}