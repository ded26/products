﻿namespace Products.Data.EntityFramework
{
    using System.Data.Entity;
    using Models;

    /// <summary>
    /// Entity framework context for code first 
    /// </summary>
    public class ProductContext : DbContext
    {
        public DbSet<Product> Products { get; set; }

        public ProductContext()
            : base("Products")
        {
            
        }
    }
}